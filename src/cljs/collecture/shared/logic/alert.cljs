(ns collecture.shared.logic.alert
  (:require [reagent.ratom :as ratom :include-macros true]
            [re-frame.core :as re-frame]))

;; Handlers

(re-frame/register-handler :set-alert-from-validation
 (fn [state [_ res]]
   (if (and (some? (:status res))
            (not= (:status res) 200)
            (not (some? (get-in res [:response :errors]))))
     (re-frame/dispatch
      [:set-alert (or (:status-text res) "Something bad happened D:")])
     (let [{{errors :errors} :response} res
           [[_ client-error-message] client-validation] (vec res)
           [[_ server-error-message] server-validation] (vec errors)]
       (if errors
         (re-frame/dispatch [:set-alert (if (set? server-error-message)
                                          (first server-error-message)
                                          errors)])
         (re-frame/dispatch [:set-alert (first client-error-message)]))))
   state))

(re-frame/register-handler :clear-alert
 (fn [state [_]]
   (assoc state :alert nil)))

;; Subscribers

(re-frame/register-sub :alert
 (fn [state _]
   (ratom/reaction (:alert @state))))
