(ns collecture.shared.logic.group
  (:require [reagent.ratom :as ratom :include-macros true]
            [re-frame.core :as re-frame]))

;; Handlers

(re-frame/register-handler :set-selected-group
  (fn [state [_ {id :id :as group}]]
    (re-frame/dispatch [:get-lectures-by-group-id id])
    (assoc state :selected-group group)))

;; Subscribers

(re-frame/register-sub :selected-group
  (fn [state _]
    (ratom/reaction (:selected-group @state))))