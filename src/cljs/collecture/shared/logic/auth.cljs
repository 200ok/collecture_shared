(ns collecture.shared.logic.auth
  (:require [reagent.ratom :as ratom :include-macros true]
            [collecture.shared.schemas.auth :as auth-schema]
            [validateur.validation :as v]
            [re-frame.core :as re-frame]
            [clojure.walk :as walk]
            [ajax.core :refer [POST PUT DELETE]]))

;; Registration

(re-frame/register-handler :sign-up
 (fn [state [_ registration-credentials]]
   (if (v/valid? auth-schema/registration-credentials-validation
                 registration-credentials)
     (POST "/api/v1/user"
           {:params        registration-credentials
            :handler       #(re-frame/dispatch [:got-signed-up
                                                (walk/keywordize-keys %1)])
            :error-handler #(re-frame/dispatch [:set-alert-from-validation
                                                (walk/keywordize-keys %1)])})
     (re-frame/dispatch [:set-alert-from-validation
                         (auth-schema/registration-credentials-validation
                          registration-credentials)]))
   state))

;; Login for token

(re-frame/register-handler :log-in-for-token
 (fn [state [_ login-credentials]]
   (if (v/valid? auth-schema/credentials-validation login-credentials)
     (PUT "/api/v1/token"
          {:params        login-credentials
           :handler       #(re-frame/dispatch [:got-token
                                               (walk/keywordize-keys %1)])
           :error-handler #(re-frame/dispatch [:set-alert-from-validation
                                               (walk/keywordize-keys %1)])})
     (re-frame/dispatch [:set-alert-from-validation
                         (auth-schema/credentials-validation login-credentials)]))
   state))

;; Logout with token

(re-frame/register-handler :log-out-with-token
 (fn [state [_]]
   (DELETE "/api/v1/token"
           {:handler       #(re-frame/dispatch [:got-logged-out])
            :error-handler #(re-frame/dispatch [:got-logged-out])})
   state))

;; Change Password

(re-frame/register-handler :set-reset-token
 (fn [state [_ token]]
   (assoc state :reset-token token)))

(re-frame/register-sub :reset-token
 (fn [state _]
   (ratom/reaction (:reset-token @state))))

(re-frame/register-handler :change-password
 (fn [state [_ change-password-credentials]]
   (if (v/valid? auth-schema/change-password-validation
                 change-password-credentials)
     (PUT "/api/v1/user/change-password"
          {:params        change-password-credentials
           :handler       #(re-frame/dispatch [:has-changed-password
                                               (walk/keywordize-keys %1)])
           :error-handler #(re-frame/dispatch [:set-alert-from-validation
                                               (walk/keywordize-keys %1)])})
     (re-frame/dispatch [:set-alert-from-validation
                         (auth-schema/change-password-validation
                          change-password-credentials)]))
   state))

;;when changing via deep-link while logged out
(re-frame/register-handler :change-password-with-token
 (fn [state [_ change-password-credentials token]]
   (if (v/valid? auth-schema/change-password-reset-token-validation
                 change-password-credentials)
     (PUT (str "/api/v1/user/change-password/" token)
          {:params        change-password-credentials
           :handler       #(re-frame/dispatch [:has-changed-password
                                               (walk/keywordize-keys %1)])
           :error-handler #(re-frame/dispatch [:set-alert-from-validation
                                               (walk/keywordize-keys %1)])})
     (re-frame/dispatch [:set-alert-from-validation
                         (auth-schema/change-password-reset-token-validation
                          change-password-credentials)]))
   state))

;; Reset Password

(re-frame/register-handler :reset-password
 (fn [state [_ email]]
   (if (v/valid? auth-schema/reset-password-email-validation email)
     (PUT "/api/v1/user/reset-password"
          {:params        email
           :handler       #(re-frame/dispatch [:has-reset-password
                                               (walk/keywordize-keys %1)])
           :error-handler #(re-frame/dispatch [:set-alert-from-validation
                                               (walk/keywordize-keys %1)])})
     (re-frame/dispatch [:set-alert-from-validation
                         (auth-schema/reset-password-email-validation email)]))
   state))
