(ns collecture.shared.logic.lecture
  (:require [reagent.ratom :as ratom :include-macros true]
            [re-frame.core :as re-frame]
            [clojure.walk :as walk]
            [ajax.core :refer [GET]]))

;; Handlers

(re-frame/register-handler :set-lectures
 (fn [state [_ lectures]]
   (assoc state :lectures lectures)))

(re-frame/register-handler :get-lectures-by-group-id
 (fn [state [_ group-id]]
   (GET (str "/api/v1/lectures/from-group/" group-id)
        {:handler       #(re-frame/dispatch [:got-lectures
                                             (walk/keywordize-keys %1)])
         :error-handler #(re-frame/dispatch [:set-alert-from-validation
                                             (walk/keywordize-keys %1)])})
   state))

;; Subscribers

(re-frame/register-sub :lectures
 (fn [state _]
   (ratom/reaction (:lectures @state))))
