(ns collecture.shared.schemas.group
  (:require [schema.core :as s]
            [validateur.validation :as v]))

(def group-validation
  (v/validation-set
   (v/presence-of :name
                  :message "Group name can't be blank.")))

;; New

(s/defschema NewGroup {:name           s/Str
                       (s/optional-key
                        :description) s/Str
                       (s/optional-key
                        :private)     s/Bool})

;; Update

(s/defschema UpdatedGroup
  (merge {:id s/Uuid}
         NewGroup))
