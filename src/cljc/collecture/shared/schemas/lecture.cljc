(ns collecture.shared.schemas.lecture
  (:require [schema.core :as s]
            [validateur.validation :as v]))

(def lecture-validation
  (v/validation-set
   (v/presence-of :name
                  :message "Lecture name can't be blank.")))

;; New

(s/defschema NewLecture {:name s/Str
                         (s/optional-key :description) s/Str})

;; Update

(s/defschema UpdatedLecture
  (merge {:id s/Uuid} NewLecture))
