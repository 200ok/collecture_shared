(ns collecture.shared.schemas.auth
  (:require [schema.core :as s]
            [validateur.validation :as v]))

(def email-validation
  (v/validation-set
   (v/presence-of :email
                  :message "Email address can't be blank")
   (v/format-of   :email
                  :format #".+@.+\..+" :allow-blank true :allow-nil true
                  :message-fn (fn [& _] "Email address doesn't look valid"))
   (v/validate-with-predicate
    :email
    (fn [values]
      (not (clojure.string/includes? (:email values)
                                "mailinator.com")))
      :message "No throwaway email addresses, please")
   (v/length-of   :email
                  :within (range -1 255)
                  :allow-blank true
                  :allow-nil true
                  :message-fn (fn [& _] "Email address can't be longer than 254 characters"))))

(def password-validation
  (v/validation-set
   (v/presence-of :password
                  :message "Password can't be blank")
   (v/length-of   :password
                  :within (range 8 255)
                  :allow-blank true
                  :allow-nil true
                  :message-fn (fn [& _] "Password can't be shorter than 8 characters"))
   (v/validate-with-predicate
    :password-confirmation
    (fn [values]
      (= (:password values) (:password-confirmation values)))
    :message "Password confirmation doesn't match password")))

;; Login & Token

(s/defschema Credentials {:email    s/Str
                          :password s/Str})

(def credentials-validation
  (v/compose-sets
   email-validation
   (v/validation-set
    (v/presence-of :password :message
                   "Password can't be blank"))))

;; Registration

(s/defschema RegistrationCredentials
  {:email                 s/Str
   :password              s/Str
   (s/optional-key :source) s/Str
   :password-confirmation s/Str})

(def registration-credentials-validation
  (v/compose-sets
   email-validation
   password-validation))

;; Reset Password

(s/defschema ResetPassword {:email s/Str})

(def reset-password-email-validation
  email-validation)

;; Change Password

(s/defschema ChangePassword
  {:current-password      s/Str
   :password              s/Str
   :password-confirmation s/Str})

(def change-password-validation
  (v/compose-sets
   password-validation
   (v/validation-set
    (v/presence-of :current-password :message
                   "Current password can't be blank"))))

(s/defschema ChangePasswordToken
  (-> ChangePassword
      (dissoc :current-password)))

(def change-password-reset-token-validation
  password-validation)
