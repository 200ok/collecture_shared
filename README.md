# collecture-shared

Shared Logic & Resources

## Logic

| Platforms     | Tech-Stack                                         |
| ------------- | -------------------------------------------------- |
| Android       | Android SDK -> React Native -> reagent -> re-frame |
| iOS           | iOS     SDK -> React Native -> reagent -> re-frame |
| Web           | Browser API -> React        -> reagent -> re-frame |

Because all platforms share
- a common programming language (cljs)
- common frameworks             (reagent & re-frame)
we are able to share common logic between all of them.

[Here](./src/collecture/shared) resides the shared logic

## Resources

Using this project (repo) as a submodule embedded in the main projects (web & mobile), we are even able to share resources.

## License

Copyright © 2016 collecture
